db.getSiblingDB("admin").auth("root", "$MONGODB_ROOT_PASSWORD" );

admin = db.getSiblingDB("admin");

admin.createUser(
  {
    user: "oplogger",
    pwd: "oplogger-password",
    roles: [ { role: "read", db: "local" } ]
  }
);
